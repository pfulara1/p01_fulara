//
//  ViewController.m
//  Hello_World
//
//  Created by Paritosh on 1/19/17.
//  Copyright © 2017 Paritosh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property(nonatomic,weak) IBOutlet UILabel *messageLabel;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)Enter:(id)sender{
    
    NSString *wordText=@"Welcome To This Crazy App!";
    self.messageLabel.text=wordText;
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"hello"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    enter.hidden=true;
    
    
    
}

@end
