//
//  AppDelegate.h
//  Hello_World
//
//  Created by Paritosh on 1/19/17.
//  Copyright © 2017 Paritosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

